<?php

/**
 * @file
 * Install, update and uninstall functions for the worldcore module.
 */

/**
 * Worldcore payments table.
 */
function worldcore_schema() {

  $schema['worldcore'] = array(
    'description' => 'The worldcore table stores information about each payment.',
    'fields' => array(
      'pid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Unique payment ID.',
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'The {users}.uid that owns this payment; initially, this is the user that created it.',
      ),
      'amount' => array(
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => TRUE,
        'default' => 0,
        'precision' => 12,
        'scale' => 6,
      ),
      'created' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Time of payment creation',
      ),
      'currency' => array(
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Currency type',
      ),
      'memo' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
        'default' => '',
      ),
      'track' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Worldcore transaction ID',
      ),
      'enrolled' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Time of payment enrolling',
      ),
      'merchant_account' => array(
        'type' => 'varchar',
        'length' => 15,
        'not null' => TRUE,
        'default' => '',
        'description' => 'WC account of merchant',
      ),
      'account' => array(
        'type' => 'varchar',
        'length' => 15,
        'not null' => TRUE,
        'default' => '',
        'description' => 'WC account of buyer',
      ),
      'error' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Text of error (if occurred)',
      ),
      'params' => array(
        'type' => 'text',
        'size' => 'normal',
        'not null' => TRUE,
        'description' => 'Additional payment params',
      ),
    ),
    'primary key' => array('pid'),
  );

  return $schema;
}

/**
 * Uninstall worldcore module function.
 */
function worldcore_uninstall() {

  path_delete(array('source' => 'worldcore/status_url'));

  $variables = array(
    'worldcore_currencies',
    'worldcore_api_key',
    'worldcore_api_password',
  );
  foreach ($variables as $variable) {
    variable_del($variable);
  }

}
