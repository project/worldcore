<?php

/**
 * @file
 * Administrative page callbacks for WorldCore module.
 */

/**
 * Worldcore payment confirmation form.
 */
function worldcore_payments_pre() {

  if (!empty($_POST['payments']) && isset($_POST['operation']) && ($_POST['operation'] == 'delete')) {
    return drupal_get_form('worldcore_payments_delete_confirm');
  }
  else {
    return drupal_get_form('worldcore_payments');
  }
}

/**
 * Worldcore payments list header.
 */
function worldcore_payments($form, &$form_state) {

  $header = array(
    'pid' => array('data' => t('ID'), 'field' => 'r.pid', 'sort' => 'desc'),
    'created' => array('data' => t('Created'), 'field' => 'r.created'),
    'user' => array('data' => t('User'), 'field' => 'r.uid'),
    'amount' => array('data' => t('Amount'), 'field' => 'r.amount'),
    'currency' => array('data' => t('Currency'), 'field' => 'r.currency'),
    'memo' => t('Memo'),
    'enrolled' => array('data' => t('Enrolled'), 'field' => 'r.enrolled'),
    'track' => array('data' => t('WC track'), 'field' => 'r.track'),
    'merchant_account' => array('data' => t('Merchant account'), 'field' => 'r.merchant_account'),
    'account' => array('data' => t('account'), 'field' => 'r.account'),
  );

  $default_settings = serialize(filter_var(_worldcore_GetDefCurSetts(), FILTER_SANITIZE_STRING));
  $serialized_settings = variable_get('worldcore_currencies', $default_settings);

  $currency_settings = filter_var(unserialize($serialized_settings), FILTER_SANITIZE_STRING);

  $select = db_select('worldcore', 'r')->extend('PagerDefault')->extend('TableSort');
  $result = $select->fields('r', array(
    'pid',
    'uid',
    'created',
    'amount',
    'currency',
    'memo',
    'enrolled',
    'track',
    'merchant_account',
    'account',
    'error',
  ))->orderByHeader($header)->limit(50)->execute();

  $payments = array();
  foreach ($result as $payment) {
    $user = array('account' => user_load($payment->uid));
    $payments[$payment->pid] = array(
      'pid' => $payment->pid,
      'created' => date("m/d/Y H:i", $payment->created),
      'user' => theme('username', $user),
      'amount' => round($payment->amount, $currency_settings[$payment->currency]['presc']),
      'currency' => $payment->currency,
      'memo' => $payment->memo,
      'enrolled' => (($payment->enrolled > 0 && $payment->error == '') ? date("m/d/Y H:i", $payment->enrolled) :
    (!empty($payment->error) ? '<small><B>Error: </B>' . $payment->error . ' (' . date("m/d/Y H:i", $payment->enrolled) . ')</smal>' : '-')),
      'track' => $payment->track,
      'merchant_account' => $payment->merchant_account,
      'account' => $payment->account,
    );
  }

  $form['payments'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $payments,
    '#empty' => t('No payments available.'),
  );
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
    '#weight' => -1,
  );
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => array('delete' => t('Delete selected'), 'enroll' => t('Enroll selected')),
    '#default_value' => 'delete',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
  );

  $form['pager'] = array('#theme' => 'pager', '#weight' => 5);
  return $form;

}

/**
 * Worldcore payments list validation form.
 */
function worldcore_payments_validate($form, &$form_state) {
  $form_state['values']['payments'] = array_filter($form_state['values']['payments']);
  if (count($form_state['values']['payments']) == 0) {
    form_set_error('', t('No payments selected.'));
  }
}

/**
 * Worldcore payments list form submit.
 */
function worldcore_payments_submit($form, &$form_state) {
  $payments = array_filter($form_state['values']['payments']);
  switch ($form_state['values']['operation']) {
    case 'enroll':
      $t = time();
      foreach ($payments as $pid) {
        if (_worldcore_enrollpayment($pid, 'MANUALLY', $t)) {
          module_invoke_all('worldcore', 'enrolled', $pid);
        }
      }
      drupal_set_message(t('The payments have been enrolled.'));
      break;
  }
}

/**
 * Worldcore payments delete confirmation form.
 */
function worldcore_payments_delete_confirm($form, &$form_state) {
  $edit = $form_state['values'];
  $form['payments'] = array('#tree' => TRUE);
  foreach (array_filter($edit['payments']) as $pid => $value) {
    $form['payments'][$pid] = array('#type' => 'hidden', '#value' => $pid);
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');

  return confirm_form(
        $form,
        t('Are you sure you want to delete selected payments?'),
        'admin/worldcore/payments', t('This action cannot be undone.'),
        t('Delete all selected'), t('Cancel')
    );
}

/**
 * Worldcore payments delete confirmation form submit.
 */
function worldcore_payments_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['payments'] as $pid => $value) {
      _worldcore_deletepayment($pid);
    }
    drupal_set_message(t('The payments have been deleted.'));
  }
  $form_state['redirect'] = 'admin/worldcore/payments';

}

/**
 * Worldcore module settings form.
 */
function worldcore_settingsform($form, &$form_state) {

  $form['API_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('worldcore_api_key', ''),
    '#size' => 40,
    '#maxlength' => 100,
    '#required' => TRUE,
  );

  $form['API_password'] = array(
    '#type' => 'textfield',
    '#title' => t('API password'),
    '#default_value' => variable_get('worldcore_api_password', ''),
    '#size' => 40,
    '#maxlength' => 100,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  return $form;

}

/**
 * Worldcore settings form submit.
 */
function worldcore_settingsform_submit(&$form, $form_state) {

  variable_set('worldcore_api_key', $form_state['values']['API_key']);

  variable_set('worldcore_api_password', $form_state['values']['API_password']);

  drupal_set_message(t("Settings has been saved."));
  drupal_goto('admin/worldcore/settings');
}

/**
 * Worldcore currencies list.
 */
function worldcore_currencies() {

  $default_currency_settings = _worldcore_GetDefCurSetts();

  $default_settings = serialize(filter_var($default_currency_settings, FILTER_SANITIZE_STRING));
  $serialized_settings = variable_get('worldcore_currencies', $default_settings);

  $currency_settings = filter_var(unserialize($serialized_settings), FILTER_SANITIZE_STRING);

  foreach ($currency_settings as $key => $value) {

    $row = array();

    $row[] = array('data' => $key);
    $row[] = array('data' => ((empty($value['account'])) ? 'n/a' : $value['account']));
    $row[] = array('data' => (($value['enabled'] == 1) ? 'yes' : 'no'));
    $row[] = array(
      'data' => l(
    t('edit'), 'admin/worldcore/currencies/edit/' . $key) . ' ' . l(
    t('delete'), 'admin/worldcore/currencies/delete/' . $key) . ' ' . l(
    t('view sample'), 'admin/worldcore/sample/' . $key),
    );

    $rows[] = $row;

  }

  // Individual table headers.
  $header = array();
  $header[] = t('Currency');
  $header[] = t('WC account');
  $header[] = t('Enabled');
  $header[] = t('Action');

  $output = theme(
        'table', array(
          'header' => $header,
          'rows' => $rows,
          'attributes' => array(),
          'caption' => '',
          'colgroups' => array(),
          'empty' => t('Please add at least one currency.'),
          'sticky' => FALSE,
        )
    );

  return $output;

}

/**
 * Worldcore currency edit form.
 */
function worldcore_currency_edit($form, &$form_state, $cur = '') {

  $default_settings = serialize(filter_var(_worldcore_GetDefCurSetts(), FILTER_SANITIZE_STRING));
  $serialized_settings = variable_get('worldcore_currencies', $default_settings);

  $currency_settings = filter_var(unserialize($serialized_settings), FILTER_SANITIZE_STRING);

  if (!empty($cur)) {
    $currency = $currency_settings[$cur];
  }

  // Interface data:
  if (!empty($cur)) {

    $form['cur'] = array(
      '#type' => 'item',
      '#title' => t('Currency'),
      '#description' => $cur,
    );

    $form['currency'] = array(
      '#type' => 'hidden',
      '#value' => $cur,
    );

  }
  else {

    $form['currency'] = array(
      '#type' => 'textfield',
      '#title' => t('Currency'),
      '#description' => t('Enter currency ID string like "USD"'),
      '#default_value' => '',
      '#size' => 10,
      '#maxlength' => 15,
      '#required' => TRUE,
    );

    $currency['account'] = '';

    $currency['presc'] = '';

    $currency['enabled'] = '';

  }

  $form['account'] = array(
    '#type' => 'textfield',
    '#title' => t('WC account'),
    '#default_value' => $currency['account'],
    '#size' => 20,
    '#maxlength' => 15,
    '#required' => FALSE,
  );

  /*		$form['presc'] = array(
  '#type' => 'textfield',
  '#title' => t('Decimals'),
  '#default_value' => $currency['presc'],
  '#size' => 5,
  '#maxlength' => 3,
  '#required' => TRUE
  );
   */
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => $currency['enabled'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  return $form;

}

/**
 * Worldcore currency edit form validation.
 */
function worldcore_currency_edit_validate($form, &$form_state) {

  if (empty($form_state['values']['currency'])) {
    form_set_error('', t('Currency ID does not specified.'));
  }

  if ($form_state['values']['enabled'] == '1') {
    if (empty($form_state['values']['account'])) {
      form_set_error('account', t('WC account can not be empty for enabled currency'));
    }
  }

  if (!empty($form_state['values']['account']) && !empty($form_state['values']['currency'])) {

    $letter = '';
    switch ($form_state['values']['currency']) {
      case 'USD': $letter = 'S';
        break;

      case 'EUR': $letter = 'E';
        break;
    }

    if ($letter != '') {

      if ($letter != substr($form_state['values']['account'], 0, 1)) {
        form_set_error('account', t('WC account must start from !letter for !currency currency', array('!letter' => $letter, '!currency' => $form_state['values']['currency'])));
      }
    }

  }

}

/**
 * Worldcore currency edit form submit.
 */
function worldcore_currency_edit_submit(&$form, &$form_state) {

  $default_settings = serialize(filter_var(_worldcore_GetDefCurSetts(), FILTER_SANITIZE_STRING));
  $serialized_settings = variable_get('worldcore_currencies', $default_settings);

  $currency_settings = filter_var(unserialize($serialized_settings), FILTER_SANITIZE_STRING);

  $currency_settings["{$form_state['values']['currency']}"] = array(
    'enabled' => $form_state['values']['enabled'],
    'account' => $form_state['values']['account'],
    'presc' => 2,
  );

  variable_set('worldcore_currencies', serialize($currency_settings));

  drupal_set_message(t('Currency has been saved.'));

  $form_state['redirect'] = 'admin/worldcore/currencies';

}

/**
 * Worldcore currency delete form.
 */
function worldcore_currency_delete($form, &$form_state, $cur) {

  $form['cur'] = array(
    '#type' => 'value',
    '#value' => t($cur),
  );

  return confirm_form(
        $form,
        t('Are you sure you want to delete %cur ?', array('%cur' => $cur)),
        'admin/worldcore/currencies',
        t('This action cannot be undone.'),
        t('Delete'),
        t('Cancel')
    );
}

/**
 * Worldcore currency delete form processing.
 */
function worldcore_currency_delete_submit($form, &$form_state) {

  if ($form_state['values']['confirm']) {

    $default_settings = serialize(filter_var(_worldcore_GetDefCurSetts(), FILTER_SANITIZE_STRING));
    $serialized_settings = variable_get('worldcore_currencies', $default_settings);

    $currency_settings = filter_var(unserialize($serialized_settings), FILTER_SANITIZE_STRING);

    $out = array();
    foreach ($currency_settings as $key => $value) {
      if ($key != $form_state['values']['cur']) {
        $out[$key] = $value;
      }
    }

    if (count($out) < count($currency_settings)) {
      variable_set('worldcore_currencies', serialize($out));
      drupal_set_message(t('Currency has been deleted.'));
    }
    else {
      drupal_set_message(t('Currency has NOT been deleted.'));
    }

  }
  else {
    drupal_set_message(t('Currency has NOT been deleted.'));
  }

  $form_state['redirect'] = 'admin/worldcore/currencies';

}

/**
 * Sample settings.
 */
function worldcore_sample() {

  global $base_path;

  return t(
        '<h1>Setting up WorldCore</h1>
<p><A HREF="https://worldcore.eu/">Login</A> to your WC account, click on <A HREF="https://worldcore.eu/Customer/ApiSettings/Merchant">Settings</A> menu section and fill in the following fields:
<table cellpadding="5">
<tbody>
<tr>
	<td nowrap="nowrap">Status URL:</td>
	<td align="left"><input style="display: inline;" value="!status_url" size="50" id="m_name" name="m_name" type="text"></td>
</tr>
<tr>
	<td nowrap="nowrap">Success URL:</td>
	<td align="left"><input style="display: inline;" value="!success_url" size="50" id="m_name" name="m_name" type="text"></td>
</tr>
<tr>
	<td nowrap="nowrap">Failure URL:</td>
	<td align="left"><input style="display: inline;" value="!fail_url" size="50" id="m_name" name="m_name" type="text"></td>
</tr>
</tbody></table>',
        array(
          '!status_url' => 'https://' . $_SERVER['HTTP_HOST'] . $base_path . drupal_get_path_alias('worldcore/status_url'),
          '!success_url' => 'https://' . $_SERVER['HTTP_HOST'] . $base_path . drupal_get_path_alias('worldcore/success', ''),
          '!fail_url' => 'https://' . $_SERVER['HTTP_HOST'] . $base_path . drupal_get_path_alias('worldcore/fail', ''),
        )
    );

}
