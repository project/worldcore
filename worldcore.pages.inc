<?php

/**
 * @file
 * User page callbacks for the WorldCore module.
 */

/**
 * Worldcore payment form.
 */
function worldcore_prefillform($form, &$form_state) {
  $form = array(); $currencies_ar = array();

  $default_settings = serialize(filter_var(_worldcore_GetDefCurSetts(), FILTER_SANITIZE_STRING));
  $serialized_settings = variable_get('worldcore_currencies', $default_settings);

  $currency_settings = filter_var(unserialize($serialized_settings), FILTER_SANITIZE_STRING);

  foreach ($currency_settings as $key => $value) {
    if ($value['enabled'] == 1) {
      $currencies_ar[$key] = $key;
    }
  }

  $form['currency'] = array(
    '#type' => 'radios',
    '#title' => t("Currency"),
    '#options' => $currencies_ar,
    '#default_value' => 'USD',
    // '#description' => t("Select payment currency."),.
    '#required' => TRUE,
  );

  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => '',
    '#size' => 10,
    '#maxlength' => 12,
    '#required' => TRUE,
  );

  $form['memo'] = array(
    '#type' => 'textarea',
    '#title' => t('Memo'),
    '#default_value' => t('Payment to !sitename', array('!sitename' => variable_get('site_name', 'Drupal'))),
    '#description' => t("Payment description."),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Cerate payment'),
  );

  return $form;
}

/**
 * Worldcore payment form submit controller.
 */
function worldcore_prefillform_submit(&$form, $form_state) {
  $payment = _worldcore_createpayment(
        array(
          'amount' => $form_state['values']['amount'],
          'currency' => $form_state['values']['currency'],
          'memo' => $form_state['values']['memo'],
        )
    );

  if (is_array($payment) && $payment['pid'] > 0) {
    drupal_set_message(t("Please confirm payment details"));
    drupal_goto('worldcore/payment/' . $payment['pid']);
  }
}

/**
 * Worldcore success payment page theming.
 */
function worldcore_success() {
  return theme('worldcore_payment_success');
}

/**
 * Worldcore payment failed page theming.
 */
function worldcore_fail() {
  return theme('worldcore_payment_fail');
}

/**
 * Worldcore status_url processing.
 */
function worldcore_status() {

  global $base_path;

  drupal_add_http_header('Content-type', 'text/html; charset=UTF-8');

  // Check url.
  $url = trim($_SERVER['REQUEST_URI'], '/');
  $alias = trim($base_path . drupal_get_path_alias('worldcore/status_url'), '/');
  if ($url != $alias) {
    watchdog('WorldCore', 'Wrong Result URL accessed "%url"', array("%url" => $url), WATCHDOG_WARNING);
    die();
  }

  $headers = apache_request_headers();
  $json_body = file_get_contents('php://input');
  $hash_check = strtoupper(hash('sha256', $json_body . variable_get('worldcore_api_password', '')));
  // Hash checking.
  if ($headers['WSignature'] == $hash_check) {

    // Some code to confirm payment.
    $created = time();

    $decoded_response = json_decode($json_body, TRUE);

    $payment = _worldcore_pid_load($decoded_response['invoiceId']);

    $_POST['track'] = $decoded_response['track'];

    _worldcore_enrollpayment($payment['pid'], $decoded_response['account'], $created);

  }
  else {

    // Some code to log invalid payments.
    watchdog('WorldCore',
    'Hash mismatch! %WSignature vs. %hash_check',
    array(
      '%WSignature' => $headers['WSignature'],
      '%hash_check' => $hash_check,
    ),
    WATCHDOG_ERROR);

    die();

  }

}
